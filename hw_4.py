# 1. Создать txt файл, в нем написать текст.
#  - Считать данные с файла и вывести в консоль
#  - Считать данные с этого же файла и записать в новый файл
#  - Считать данные с этого же файла, преобразовать (любые операции) и записать в этот же файл с разделителем
#    (пробел, точка, запятая и тп)

def file_read():
    file = open("read.txt", "r")
    a = file.read()
    file.close()
    return a
print(file_read())

def file_write():
    file = open("read.txt", "r")
    a = file.read()
    file.close()
    file = open("write.txt", "w")
    file.write(a)
    file.close()
file_write()

def etot_je_file():
    file = open("read.txt", "r")
    a = file.read()
    file.close()
    file = open("read.txt", "a")
    file.write(",")
    file.write(a.replace("o", "a"))
    file.close()
etot_je_file()

# 2. Преобразовать дату. Нужно написать код, который из Feb 12 2019 2:41PM сделает 2019-02-12 14:41:00

import time

a = time.strftime("%Y-%d-%m %H:%M:%S", time.strptime('Feb 12 2019 2:41PM', '%b %d %Y %I:%M%p'))

print(a)

# 3. Напишите программу, которая принимает год, и возвращает список дат всех понедельников в данном году.
#    Работа с датами (можно использовать любые модули и гуглить)

import calendar
import datetime


def main(year):
    data = []
    calendar.weekday(year, 1, 1)
    monke = 7 - calendar.weekday(year, 1, 1)
    date =  datetime.datetime(year, 1, 1 + monke)
    while date.year == year:
        data.append(date.strftime('%d-%m-%Y'))
        date += datetime.timedelta(days=7)
    return data
print(main(2022))
